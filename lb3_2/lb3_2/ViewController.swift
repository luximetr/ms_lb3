//
//  ViewController.swift
//  lb3_2
//
//  Created by Александр on 09.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, URLSessionDelegate, URLSessionDownloadDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var downloadButton: UIButton!
    
    //MARK: Fields
    var topics: Array<SMXMLElement> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.text = "http://www.vesti.ru/vesti.rss"
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.topics.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell()
        }
        let item = topics[indexPath.row]
        cell?.textLabel?.text = item.childNamed("title").value
        return cell!
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.topics[indexPath.row]
        let urlString = item.childNamed("link").value
        let url = URL(string: urlString!)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    //MARK: IBActions
    @IBAction func actionTapOndownloadButton(_ sender: Any) {
        if let url = URL(string: self.textField.text!) {
            let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
            
            let task = session.downloadTask(with: url)
            task.resume()
        }
    }
    
    //MARK: URLSessionDownloadDelegate
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        do {
            let data = try Data(contentsOf: location)
            let xmlDocument = try SMXMLDocument(data: data)
            self.topics = xmlDocument.childNamed("channel").childrenNamed("item") as! Array<SMXMLElement>
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        catch {
        }
    }
}

