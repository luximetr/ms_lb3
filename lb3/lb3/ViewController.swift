//
//  ViewController.swift
//  lb3
//
//  Created by Александр on 07.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, URLSessionDownloadDelegate {
    //MARK: IBOutlets
    
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imageURLTextField: UITextField!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.hidesWhenStopped = true
        self.restoreInitialViewState()
        self.imageURLTextField.text = "https://upload.wikimedia.org/wikipedia/commons/0/08/Westerhever_Nordostblick.jpg"
    }
    
    //MARK: Private
    func restoreInitialViewState() {
        self.progressView.progress = 0
        self.percentLabel.text = "0%"
        self.progressView.isHidden = true
        self.percentLabel.isHidden = true
        self.downloadButton.isEnabled = true
    }
    
    //MARK: IBActions
    
    @IBAction func actionTapOnDownloadButton(_ sender: Any) {
        if let url = URL(string: self.imageURLTextField.text!) {
            self.downloadButton.isEnabled = false
            self.activityIndicator.startAnimating()
            self.progressView.isHidden = false
            self.percentLabel.isHidden = false
            
            let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
            
            let task = session.downloadTask(with: url)
            task.resume()
        }
    }
    
    //MARK: URLSessionDownloadDelegates
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        do {
            let data = try Data(contentsOf: location)
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.imageView.image = image
                self.percentLabel.text = "100%"
                self.progressView.progress = 1
                self.activityIndicator.stopAnimating()
                self.restoreInitialViewState()
                UIView.animate(withDuration: 0.3, animations: {
                    self.placeholderImageView.alpha = 0
                })
            }
        }
        catch {
        }
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        DispatchQueue.main.async {
            self.percentLabel.text = "\(Int((Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)) * 100))%"
            self.progressView.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        }
        
    }
    
}

